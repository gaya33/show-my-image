require 'instagram'

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # before_filter :set_code
  # before_filter :set_access_token

  # def set_access_token
  #   if params[:access_token].present?
  #     @access_token = params[:access_token]
  #   end
  # end


  private

  # def set_code
  #   if params[:set_code].present?
  #     @code = params[:set_code]
  #   end
  # end

  def configure_instagram
    Instagram.configure do |config|
      config.client_id = Settings.show_my_image.client_id
      config.client_secret = Settings.show_my_image.client_secret
    end
  end
  
  def get_access_token
    begin
      response = Instagram.get_access_token(params[:code], :redirect_uri => Settings.show_my_image.callback_url)
      @access_token = response[:access_token]
      @user = response[:user]
    rescue => e
      # redirect_to sign in page since same code can not be used twice to get the access_token for instagram apis, for security purposes.
      redirect_to Rails.application.routes.url_helpers.root_path
    end
  end
end
