class AuthenticationController < ApplicationController

  before_action :configure_instagram, only: [:main]

  # will accept code param returned by the auth url of instagram on successfully authenticating
  # if code not present, it will post to the auth url with the client_id and the redirect_url
  # if code present, it will redirect to the access_token url with: client_id, client_secret, 
  # grant_type, redirect_uri(same as above), code
  def main
    if params[:code].present?
      get_access_token
    end
  end

  def sign_out
    @access_token = nil
    @code =  nil
    redirect_to Rails.application.routes.url_helpers.root_path
  end

end 