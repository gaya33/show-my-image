class ShowImageController < ApplicationController
  before_action :configure_instagram, only: [:index, :fetch_images]

  def index
  end

  def fetch_images
    if params[:access_token].present?
      client = Instagram.client(:access_token => params[:access_token])
      @user = client.user
      @media_items = client.user_recent_media
    else
      redirect_to Rails.application.routes.url_helpers.root_path
    end
  end

  def

  def privacy_policy
  end
end