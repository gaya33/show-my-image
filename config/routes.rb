Rails.application.routes.draw do

  root 'show_image#index'
  get 'main', to: 'authentication#main', as: :main
  get 'sign_out', to: 'authentication#sign_out', as: :sign_out
  get 'fetch_images', to: 'show_image#fetch_images', as: :recent_images
  get 'privacy_policy', to: 'show_image#privacy_policy'
end
