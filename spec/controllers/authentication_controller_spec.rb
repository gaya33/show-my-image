require 'rails_helper'

RSpec.describe AuthenticationController, type: :controller do

  describe "GET #main" do
    
    it "calls main with the code from the auhtorization url" do
      result = SecureRandom.hex
      should receive(:configure_instagram).and_return({code: result})
      should receive(:get_access_token)
      get :main, code: result
    end

    it "get_access_token should not be called when invoked without code" do
      should receive(:configure_instagram)
      should receive(:get_access_token).never
      get :main
    end

  end

  describe "GET #sign_out" do

    it "redirects to the root_path on signing out" do
      get :sign_out
      expect(response).to redirect_to(Rails.application.routes.url_helpers.root_path)
    end
    
  end
end