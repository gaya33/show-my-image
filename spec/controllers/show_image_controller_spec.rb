require 'rails_helper'

RSpec.describe ShowImageController, type: :controller do

  describe "GET #index" do
    
    it "calls configure_instagram before calling index" do
      should receive(:configure_instagram)
      get :index
    end

    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end

    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end
end